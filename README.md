> 如果你还未使用过该项目，请通过[该链接](https://github.com/hengxuZ/binance-quantization)，查看第一版本。有助于你更好的了解本项目。 

## 🎉第三版🎉 双向网格并且趋势开单（合约多空双向网格，带趋势对冲）
> 背景：在量化运行的过程中，每当价格回落到满足买入价格时，🤖️ 会主动下一个买入订单。如果一直📉 那么就一直买一直买，直到账户没有💰 或 设置了停止补仓的策略。
> 那么在这个价格下跌过程中，为什么不去利用下这个下跌的过程。而是坐以待毙等着上涨盈利？我们完全可以收集下跌过程中的势能，转化为我们需要的能量。

温馨提示：当前策略在震荡行情可盈利丰厚，但是出现单边趋势。可能会有较大浮亏。
如果对行情把握不准，请使用v2版本现货趋势做多。无脑看多模式。

![cxpgyV.png](https://z3.ax1x.com/2021/04/25/cxpgyV.png)

当前版本优势点🤩

1.行情处于震荡阶段时，开单多空双向都可盈利。

2.行情单边走向（无论多空），系统会自动开启对冲，为逆势方向的订单开启同等数量的量，保障在趋势到来后，避免单边亏损严重！

3.智能开单，不会根据固定价格去开单，如果发现当前有小幅度的趋势，会提升开仓点位。达到智能且点位更好的开单效果。

4.当资金费率高的时候，不用永续合约做多，避免超高的资金费率开销(假设：btc的资金费率为0.1%，那么每天需要为btc的仓位支付0.3%的费率，这是一笔极大的开销)

如果您想体验本款策略，请使用我的币安邀请码注册账号
点击->[邀请码](https://www.binancezh.io/zh-CN/register?ref=OW7U53AB)手续费返还20%。注册送10元，实名交易再送10元。提交id号给我，通过微信红包。
```
{
    "runBet": {
        "spot_buy_price": 5,                 #现货开仓价
        "spot_sell_price": 5.5,              #现货卖出价
        "spot_step": 0,                      #现货持有仓位手数
        "future_buy_price": 5.5,             #合约空单卖仓价
        "future_sell_price": 5,              #合约空单买入价
        "future_step": 0                     #合约空单持有仓位手数
    },
    "config": {
        "profit_ratio": 2,                   #价格向上百分比
        "double_throw_ratio": 2,             #价格向下百分比
        "cointype": "EOSUSDT",               #交易对
        "hedging": 0,                        # 0->震荡 1->趋势多 11->开启了空单对冲 -1->趋势空 -11->开启了多单对冲
        "spot_quantity": [
            5                                #现货购买数量 
        ],
        "future_quantity": [
            5                                # 合约空买入量
        ]
    }
}
```


并且一对一指导使用该款策略。
![一对一指导](https://z3.ax1x.com/2021/04/25/cxPtQ1.jpg)

