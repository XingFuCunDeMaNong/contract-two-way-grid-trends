# -*- coding: utf-8 -*-
from app.BinanceAPI import BinanceAPI
from app.authorization import api_key,api_secret
from data.runBetData import RunBetData
from app.dingding import Message
from data.calcIndex import CalcIndex
import time

binan = BinanceAPI(api_key,api_secret)
runbet = RunBetData()
msg = Message()
index = CalcIndex()
class Run_Main():

    def __init__(self):
        self.coinType = runbet.get_cointype()  # 交易币种
        self.profitRatio = runbet.get_profit_ratio() # 止盈比率
        self.doubleThrowRatio = runbet.get_double_throw_ratio() # 补仓比率
        pass

    def loop_run(self):
        while True:

            spot_buy_price = runbet.get_spot_buy_price()  # 现货买入价格
            spot_sell_price = runbet.get_spot_sell_price() # 现货卖出价格
            spot_quantity = runbet.get_spot_quantity()  # 现货买入量
            spot_step = runbet.get_spot_step()  # 当前现货步数(手数)

            future_buy_price = runbet.get_future_buy_price()  # 现货买入价格
            future_sell_price = runbet.get_future_sell_price() # 现货卖出价格
            future_quantity = runbet.get_future_quantity()   # 期货买入量
            future_step = runbet.get_future_step() # 当前期货步数(手数)

            cur_market_price = binan.get_ticker_price(runbet.get_cointype())  # 当前交易对市价
            right_size = len(str(cur_market_price).split(".")[1])
            trend_status = index.set_trendStatus(self.coinType, "1d", right_size) # 趋势状态值


            # 开多
            if trend_status != -1 and spot_buy_price >= cur_market_price and index.calcAngle(self.coinType,"5m",False,right_size):   # 做多买入,趋势正在拉升，不买入

                res = msg.open_buy_market_msg(self.coinType, spot_quantity) # 现货买入
                
                if res['orderId']: # 挂单成功
                    time.sleep(1)
                    runbet.set_spot_step(spot_step+1)
                    runbet.set_ratio(self.coinType)
                    runbet.modify_spot_price(cur_market_price) #修改data.json中价格、当前步数
                    time.sleep(60*0.5) # 挂单后，停止运行1分钟
                else:
                    break
            # 开空
            elif trend_status != 1 and future_buy_price <= cur_market_price and index.calcAngle(self.coinType, "5m",True,right_size):  # 是否满足卖出价

                future_res = msg.open_sell_market_msg(self.coinType, future_quantity)  # 期货买入开空
                if future_res['orderId']:
                    time.sleep(1)
                    runbet.set_ratio(self.coinType)
                    runbet.modify_future_price(cur_market_price)  # 修改data.json中价格
                    runbet.set_future_step(future_step + 1)
                    time.sleep(60 * 1)  # 挂单后，停止运行1分钟
                else:
                    break
            # 平多
            elif spot_sell_price <= cur_market_price and index.calcAngle(self.coinType,"5m",True,right_size):  # 是否满足卖出价
                
                if spot_step > 0 :
                    spot_res = msg.do_buy_market_msg(self.coinType,runbet.get_spot_quantity(False)) # 期货卖出开多

                    if spot_res['orderId']:
                        # position_price = index.get_position_price()
                        # price = cur_market_price if position_price == False else position_price
                        runbet.set_ratio(self.coinType)
                        runbet.modify_spot_price(cur_market_price) #修改data.json中价格
                        runbet.set_spot_step(spot_step - 1)  # 挂卖单,仓位 -1
                        time.sleep(60*0.5)  # 挂单后，停止运行1分钟
                    else:
                        break
                else:
                    runbet.modify_spot_price(cur_market_price)
            # 平空
            elif future_sell_price >= cur_market_price and index.calcAngle(self.coinType, "5m", False,right_size):  # 是否满足卖出价

                if future_step > 0:
                    future_res = msg.do_sell_market_msg(self.coinType, runbet.get_future_quantity(False))  # 期货卖出开多
                    if future_res['orderId']:
                        # position_price = index.get_position_price(False)
                        # price = cur_market_price if position_price == False else position_price
                        runbet.set_ratio(self.coinType)
                        runbet.modify_future_price(cur_market_price)  # 修改data.json中价格
                        runbet.set_future_step(future_step - 1)  # 挂卖单,仓位 -1
                        time.sleep(60 * 0.5)  # 挂单后，停止运行1分钟
                    else:
                        break
                else:
                    runbet.modify_future_price(cur_market_price)

            time.sleep(2) # 为了不被币安api请求次数限制


if __name__ == "__main__":
    instance = Run_Main()
    try:
        instance.loop_run()
    except Exception as e:
        error_info = "报警：币种{coin},服务停止.错误原因{info}".format(coin=instance.coinType,info=str(e))
        msg.dingding_warn(error_info)


#调试看报错运行下面，正式运行用上面
# if __name__ == "__main__":
#
#   instance = Run_Main()
#   instance.loop_run()

